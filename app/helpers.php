<?php

use Carbon\Carbon;

function adminlte_asset($path)
{
    return config('custom.AdminLTEassetsPath') . $path;
}

function formatDate($date, $oldFormat = 'Y-m-d', $newFormat = 'd.m.Y')
{
    $date = substr(trim($date), 0, 10);
    return empty($date) ? null : Carbon::createFromFormat($oldFormat, $date)->format($newFormat);
}