<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider,
    Illuminate\Support\Facades\View,
    App\Models\Rate;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {

    }


    public function register()
    {
        View::composer(['AdminLTE.forms.*', 'AdminLTE.pages.*'], function ($view) {
            $view->with('rate', Rate::first()->rate);
        });
    }
}
