<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request,
    App\Models\Category;

/**
 * Class CategoriesController - controller working with products categories
 */
class CategoriesController extends Controller
{
    // Show categories list
    public function index()
    {
        $parentCategories = Category::where('parent_id', 0)->get();

        $data = [
            'title' => 'Категории',
            'parentCategories' => $parentCategories,
        ];

        return view('AdminLTE.pages.categories.index', $data);
    }

    // Show category creating form
    public function create()
    {
        $parentCategories = Category::where('parent_id', 0)->get();

        $data = [
            'title' => 'Создание категории',
            'parentCategories' => $parentCategories,
        ];

        return view('AdminLTE.pages.categories.create-edit', $data);
    }

    // Store newly created category to the database
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'parent_id' => 'required',
        ]);

        try {
            Category::create($request->except('button_type'));

            return redirect()
                ->route('categories.index')
                ->with('message', __('categories.creation_success'))
                ->with('status', 'success');
        } catch (\Exception $e) {
            return redirect()
                ->route('categories.index')
                ->with('message', __('categories.creation_failure'))
                ->with('status', 'danger');
        }
    }

    // Show category editing form
    public function edit(Category $category)
    {
        $parentCategories = Category::where('parent_id', 0)->get();

        $data = [
            'title' => 'Редактирование категории',
            'category' => $category,
            'parentCategories' => $parentCategories,
        ];

        return view('AdminLTE.pages.categories.create-edit', $data);
    }

    // Store updated category to the database
    public function update(Category $category, Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'parent_id' => 'required',
        ]);

        try {
            $category->update($request->except('button_type'));

            if ($request->button_type === 'save') {
                return redirect()
                    ->route('categories.index')
                    ->with('message', __('categories.updating_success'))
                    ->with('status', 'success');
            } elseif ($request->button_type === 'apply') {
                return redirect()->back()
                    ->with('message', __('categories.updating_success'))
                    ->with('status', 'success');
            }
        } catch (\Exception $e) {
            if ($request->button_type === 'save') {
                return redirect()
                    ->route('categories.index')
                    ->with('message', __('categories.updating_failure'))
                    ->with('status', 'danger');
            } elseif ($request->button_type === 'apply') {
                return redirect()->back()
                    ->with('message', __('categories.updating_failure'))
                    ->with('status', 'danger');
            }
        }
    }

    // Soft delete category from the database
    public function destroy(Category $category, Request $request)
    {
        $subcategories = $category->subcategories()->get();

        if ($subcategories->count() !== 0) {
            $message = __('categories.subcategories_existing_warning', [
                'category_name' => $category->name
            ]);

            return redirect()
                ->route('categories.index')
                ->with('message', $message)
                ->with('status', 'danger');
        } else {
            try {
                $category->delete();

                return redirect()
                    ->route('categories.index')
                    ->with('message', __('categories.deleting_success'))
                    ->with('status', 'success');
            } catch (\Exception $e) {
                return redirect()
                    ->route('categories.index')
                    ->with('message', __('categories.deleting_failure'))
                    ->with('status', 'danger');
            }
        }
    }

    public function getSubcategories(Category $category)
    {
        return $category->subcategories()->get();
    }
}

