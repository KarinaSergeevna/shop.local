<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request,
    App\Models\Organization;

/**
 * Class OrganizationsController - controller working with organizations
 */
class OrganizationsController extends Controller
{
    // Show organizations list
    public function index()
    {
        $data = [
            'title' => 'Организации',
            'organizations' => Organization::all(),
        ];

        return view('AdminLTE.pages.organizations.index', $data);
    }

    // Show organization creating form
    public function create()
    {
        $data = [
            'title' => 'Создание организации',
        ];

        return view('AdminLTE.pages.organizations.create-edit', $data);
    }

    // Store newly created organization to the database
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:7|max:200|unique:organizations,name',
            'document' => 'required|min:7|max:200',
        ]);

        try {
            Organization::create($request->except('button_type'));

            return redirect()
                ->route('organizations.index')
                ->with('message', __('organizations.creation_success'))
                ->with('status', 'success');
        } catch (\Exception $e) {
            return redirect()
                ->route('organizations.index')
                ->with('message', __('organizations.creation_failure'))
                ->with('status', 'danger');
        }
    }

    // Show organization editing form
    public function edit(Organization $organization)
    {
        $data = [
            'title' => 'Редактирование организации',
            'organization' => $organization,
        ];

        return view('AdminLTE.pages.organizations.create-edit', $data);
    }

    // Store updated organization to the database
    public function update(Organization $organization, Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:7|max:200|unique:organizations,name,' . $organization->id,
            'document' => 'required|min:7|max:200',
        ]);

        try {
            $organization->update($request->except('button_type'));

            if ($request->button_type === 'save') {
                return redirect()
                    ->route('organizations.index')
                    ->with('message', __('organizations.updating_success'))
                    ->with('status', 'success');
            } elseif ($request->button_type === 'apply') {
                return redirect()->back()
                    ->with('message', __('organizations.updating_success'))
                    ->with('status', 'success');
            }
        } catch (\Exception $e) {
            if ($request->button_type === 'save') {
                return redirect()
                    ->route('organizations.index')
                    ->with('message', __('organizations.updating_failure'))
                    ->with('status', 'danger');
            } elseif ($request->button_type === 'apply') {
                return redirect()->back()
                    ->with('message', __('organizations.updating_failure'))
                    ->with('status', 'danger');
            }
        }
    }

    // Delete organization from the database
    public function destroy(Organization $organization, Request $request)
    {
        try {
            $organization->delete();

            return redirect()
                ->route('organizations.index')
                ->with('message', __('organizations.deleting_success'))
                ->with('status', 'success');
        } catch (\Exception $e) {
            return redirect()
                ->route('organizations.index')
                ->with('message', __('organizations.deleting_failure'))
                ->with('status', 'danger');
        }
    }
}
