<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request,
    App\Models\Check,
    App\Models\Product,
    App\Models\Order,
    Carbon\Carbon,
    Storage,
    Excel;

class ChecksController extends Controller
{
    public function index()
    {
        $checksActive = Check::all();
        $checksDeleted = Check::onlyTrashed()->get();

        $data = [
            'title' => 'Чеки',
            'checksActive' => $checksActive,
            'checksDeleted' => $checksDeleted,
        ];

        return view('AdminLTE.pages.checks.index', $data);
    }

    public function one(Check $check)
    {
        $checkOrders = Order::where('check_id', $check->id)->get();

        $data = [
            'title' => 'Просмотр чека №' . $check->id,
            'checkOrders' => $checkOrders,
            'checkPath' => $this->getFilePath($check->id, 'checks', 'check'),
            'orderPath' => $this->getFilePath($check->id, 'orders', 'order'),
            'invoicePath' => $this->getFilePath($check->id, 'invoices', 'invoice'),
        ];

        return view('AdminLTE.pages.checks.one', $data);
    }

    public function create()
    {
        $products = Product::where('quantity', '>', 0)->get();

        $data = [
            'title' => 'Добавление чека',
            'products' => $products,
            'checkPath' => null
        ];

        return view('AdminLTE.pages.checks.create-edit', $data);
    }

    public function store(Request $request)
    {
        if (count($request->items) > 0) {
            $check = Check::create();

            foreach ($request->items as $item) {
                $product = Product::find($item['product_id']);
                if ($product) {
                    $order = Order::where('check_id', $check->id)
                        ->where('product_id', $item['product_id'])
                        ->first();

                    if (!$order) {
                        Order::create([
                            'check_id' => $check->id,
                            'product_id' => $item['product_id'],
                            'quantity' => $item['quantity'],
                            'purchase_price' => $item['price'],
                        ]);
                    } else {
                        $order->quantity += $item['quantity'];
                        $order->save();
                    }

                    $product->quantity -= $item['quantity'];
                    $product->save();
                }
            }

            $this->formCheck($check->id);

            return $this->getCheckPath($check->id);
        }
    }

    public function edit(Check $check)
    {
        $products = Product::where('quantity', '>', 0)->get();
        $orders = Order::where('check_id', $check->id)->get();

        $data = [
            'title' => 'Редактирование чека',
            'products' => $products,
            'orders' => $orders,
            'check' => $check,
            'checkPath' => $this->getCheckPath($check->id),
        ];

        return view('AdminLTE.pages.checks.create-edit', $data);
    }

    public function update(Check $check, Request $request)
    {
        if (count($request->items) > 0) {
            foreach ($request->items as $item) {
                $product = Product::find($item['product_id']);
                if ($product) {
                    $order = Order::where('check_id', $check->id)
                        ->where('product_id', $item['product_id'])
                        ->first();

                    if (!$order) {
                        Order::create([
                            'check_id' => $check->id,
                            'product_id' => $item['product_id'],
                            'quantity' => $item['quantity'],
                            'purchase_price' => $item['price'],
                        ]);

                        $product->quantity -= $item['quantity'];
                        $product->save();
                    } else {
                        if ($order->quantity > $item['quantity']) {
                            $product->quantity += ($order->quantity - $item['quantity']);
                        } elseif ($order->quantity < $item['quantity']) {
                            $product->quantity -= ($item['quantity'] - $order->quantity);
                        }
                        $product->save();

                        $order->quantity = $item['quantity'];
                        if ($order->purchase_price < $item['price']) {
                            $order->purchase_price = $item['price'];
                        } elseif ($order->purchase_price > $item['price']) {
                            if ($item['price'] >= $product->price_sales_minimal_uan) {
                                $order->purchase_price = $item['price'];
                            } else {
                                $order->purchase_price = $product->price_sales_minimal_uan;
                            }
                        }
                        $order->save();
                    }
                }
            }

            $check->touch();

            $this->formCheck($check->id);
        }
    }

    public function toggle($id)
    {
        $check = Check::withTrashed()
            ->where('id',  $id)
            ->first();

        if ($check) {
            if ($check->trashed()) {
                $check->restore();
                return redirect()
                    ->route('checks.index')
                    ->with('status', __('users.restoration_success'));
            } else {
                $check->delete();
                return redirect()
                    ->route('checks.index')
                    ->with('status', __('users.deletion_success'));
            }
        }

        return redirect()
            ->route('checks.index')
            ->with('status', __('users.search_failure'));
    }

    public function formCheck($id)
    {
        $check = Check::withTrashed()
            ->where('id',  $id)
            ->first();

        if ($check) {
            $newName = 'check_№' . $check->id . '.xlsx';

            if (!Storage::exists($newName)) {
                Storage::copy('check.xlsx', $newName);
            }

            $checkDate =  Carbon::createFromFormat('Y-m-d H:i:s', $check->updated_at)
                ->format('d.m.Y');

            Excel::load(storage_path("app/public/$newName"), function($file) use ($check, $checkDate) {
                $file->sheet('sheet1', function($sheet) use($check, $checkDate) {
                    $sheet->setColumnFormat([
                        'AV' => '0.00'
                    ]);

                    $sheet->setBorder('AJ5', 'thin');
                    $sheet->setBorder('AV19:AV38', 'thin');

                    $sheet->cell('AJ5', $check->id);
                    $sheet->cell('AJ10', $checkDate);

                    $orders = $check->orders;
                    $rowCounter = 19;
                    $productsCounter = 1;

                    foreach ($orders as $order) {
                        $sheet->cell("A$rowCounter", $productsCounter);
                        $sheet->cell("E$rowCounter", $order->product->name);
                        $sheet->cell("AD$rowCounter", $order->product->unit->name);
                        $sheet->cell("AJ$rowCounter", $order->quantity);
                        $sheet->cell("AP$rowCounter", $order->purchase_price);
                        $sheet->cell("AV$rowCounter", $order->purchase_price * $order->quantity);
                        $rowCounter++;
                        $productsCounter++;
                    }

                    $sheet->cell("AV39", $check->getSum());
                });
            })->store('xls', storage_path('app/public/checks/' . $checkDate));

            Storage::delete($newName);
        }
    }

    public function formOrder($id)
    {
        $check = Check::withTrashed()
            ->where('id',  $id)
            ->first();

        if ($check) {
            $newName = 'order_№' . $check->id . '.xlsx';

            if (!Storage::exists($newName)) {
                Storage::copy('order.xls', $newName);
            }

            $checkDate =  Carbon::createFromFormat('Y-m-d H:i:s', $check->updated_at)
                ->format('d.m.Y');

            Excel::load(storage_path("app/public/$newName"), function($file) use ($check, $checkDate) {
                $file->sheet('sheet1', function($sheet) use($check, $checkDate) {
                    $sheet->setColumnFormat([
                        'AV' => '0.00'
                    ]);

                    $sheet->setBorder('AJ5', 'thin');
                    $sheet->setBorder('AV30:AV50', 'thin');

                    $sheet->cell('AJ5', 'test');
                    $sheet->cell('AJ10', 2);
                    $sheet->cell('H20', 'test');

                    $orders = $check->orders;
                    $rowCounter = 30;
                    $productsCounter = 1;

                    foreach ($orders as $order) {
                        $sheet->cell("A$rowCounter", $productsCounter);
                        $sheet->cell("E$rowCounter", $order->product->name);
                        $sheet->cell("AD$rowCounter", $order->product->unit->name);
                        $sheet->cell("AJ$rowCounter", $order->quantity);
                        $sheet->cell("AP$rowCounter", $order->purchase_price);
                        $sheet->cell("AV$rowCounter", $order->purchase_price * $order->quantity);
                        $rowCounter++;
                        $productsCounter++;
                    }

                    $sheet->cell("AV52", $check->getSum());
                });
            })->store('xls', storage_path('app/public/orders/' . $checkDate));

            Storage::delete($newName);
        }
    }

    public function formInvoice($id)
    {
        $check = Check::withTrashed()
            ->where('id',  $id)
            ->first();

        if ($check) {
            $newName = 'invoice_№' . $check->id . '.xlsx';

            if (!Storage::exists($newName)) {
                Storage::copy('invoice.xls', $newName);
            }

            $checkDate =  Carbon::createFromFormat('Y-m-d H:i:s', $check->updated_at)
                ->format('d.m.Y');

            Excel::load(storage_path("app/public/$newName"), function($file) use ($check, $checkDate) {
                $file->sheet('sheet1', function($sheet) use($check, $checkDate) {
                    $sheet->setColumnFormat([
                        'AV' => '0.00'
                    ]);

                    $sheet->setBorder('AJ5', 'thin');
                    $sheet->setBorder('AV30:AV43', 'thin');

                    $sheet->cell('AJ5', 'test');
                    $sheet->cell('AJ10', 2);
                    $sheet->cell('H20', 'test');

                    $orders = $check->orders;
                    $rowCounter = 21;

                    foreach ($orders as $order) {
                        $sheet->cell("A$rowCounter", $order->product->name);
                        $sheet->cell("AD$rowCounter", $order->product->unit->name);
                        $sheet->cell("AJ$rowCounter", $order->quantity);
                        $sheet->cell("AP$rowCounter", $order->purchase_price);
                        $sheet->cell("AV$rowCounter", $order->purchase_price * $order->quantity);
                        $rowCounter++;
                    }

                    $sheet->cell("AV43", $check->getSum());

                    $sheet->setBorder('AJ5', 'thin');
                });
            })->store('xls', storage_path('app/public/invoices/' . $checkDate));

            Storage::delete($newName);
        }
    }

    public function getFilePath($id, $section, $type)
    {
        $check = Check::withTrashed()
            ->where('id', $id)
            ->first();

        $subdirectory = Carbon::createFromFormat('Y-m-d H:i:s', $check->updated_at)
            ->format('d.m.Y');

        $path = asset("storage\\" . $section . "\\" . $subdirectory . "\\" . $type . "_№" . $id . ".xls");

        return $path;
    }
}
