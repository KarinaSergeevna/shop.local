<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request,
    App\Models\Product,
    App\Models\Category,
    App\Models\Unit;

/**
 * Class ProductsController - controller working with products
 */
class ProductsController extends Controller
{
    // Show products list
    public function index()
    {
        $allProducts = Product::with(['category', 'unit']);
        $productsInStock = $allProducts->where('quantity', '>', 0)->get();
        $productsMissing = $allProducts->where('quantity',  0)->get();

        $data = [
            'title' => 'Товары',
            'productsInStock' => $productsInStock,
            'productsMissing' => $productsMissing,
        ];
        return view('AdminLTE.pages.products.index', $data);
    }

    // Show product creating form
    public function create()
    {
        $data = [
            'title' => 'Добавление товара',
            'units' => Unit::all(),
            'parentCategories' => Category::getParentCategories()->get(),
        ];
        return view('AdminLTE.pages.products.create-edit', $data);
    }

    // Store newly created product to the database
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:products,name',
            'quantity' => 'required|numeric',
            'purchase_usd_rate' => 'required|numeric',
            'price_purchase_usd' =>'numeric',
            'price_purchase_uan' => 'required|numeric',
            'price_sales_minimal_uan' => 'required|numeric',
            'category_id' => 'required|numeric',
            'unit_id' => 'required|numeric',
        ]);

        $request->merge([
            'category_id' => ($request->subcat && $request->subcat !== 'all') ? $request->subcat : $request->category_id,
        ]);

        try {
            Product::create($request->except([
                'percent',
                'subcat',
                'button_type'
            ]));

            return redirect()
                ->route('products.index')
                ->with('message', __('products.creation_success'))
                ->with('status', 'success');
        } catch (\Exception $e) {
            return redirect()
                ->route('products.index')
                ->with('message', __('products.creation_failure'))
                ->with('status', 'danger');
        }
    }

    // Show product editing form
    public function edit(Product $product)
    {
        $data = [
            'title' => 'Редактирование товара',
            'product' => $product,
            'units' => Unit::all(),
            'parentCategories' => Category::getParentCategories()->get(),
        ];

        return view('AdminLTE.pages.products.create-edit', $data);
    }

    // Store updated product to the database
    public function update(Product $product, Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:products,name,' . $product->id,
            'quantity' => 'required|numeric',
            'purchase_usd_rate' => 'required|numeric',
            'price_purchase_usd' =>'numeric',
            'price_purchase_uan' => 'required|numeric',
            'price_sales_minimal_uan' => 'required|numeric',
            'category_id' => 'required|numeric',
            'unit_id' => 'required|numeric',
        ]);

        $request->merge([
            'category_id' => ($request->subcat && $request->subcat !== 'all') ? $request->subcat : $request->category_id,
        ]);

        try {
            $product->update($request->except([
                'percent',
                'subcat',
                'button_type'
            ]));

            if ($request->button_type === 'save') {
                return redirect()
                    ->route('products.index')
                    ->with('message', __('products.updating_success'))
                    ->with('status', 'success');
            } elseif ($request->button_type === 'apply') {
                return redirect()->back()
                    ->with('message', __('products.updating_success'))
                    ->with('status', 'success');
            }
        } catch (\Exception $e) {
            if ($request->button_type === 'save') {
                return redirect()
                    ->route('products.index')
                    ->with('message', __('products.updating_failure'))
                    ->with('status', 'danger');
            } elseif ($request->button_type === 'apply') {
                return redirect()->back()
                    ->with('message', __('products.updating_failure'))
                    ->with('status', 'danger');
            }
        }
    }

    // Soft delete product from the database
    public function destroy(Product $product)
    {
        try {
            $product->delete();

            return redirect()
                ->route('products.index')
                ->with('message', __('product.deleting_success'))
                ->with('status', 'success');
        } catch (\Exception $e) {
            return redirect()
                ->route('products.index')
                ->with('message', __('product.deleting_failure'))
                ->with('status', 'danger');
        }
    }
}
