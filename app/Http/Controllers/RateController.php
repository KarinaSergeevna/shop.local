<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request,
    App\Models\Rate;

class RateController extends Controller
{
    public function update()
    {
        $rate = Rate::first();
        $data = [
            'rate' => $rate,
        ];

        return view('pages.update_rate', $data);
    }

    public function updatePost(Request $request)
    {
        $rate = Rate::first();

        $this->validate($request, [
            'rate' => 'required|numeric',
        ]);

        $rate->update([
            'rate' => $request->rate,
        ]);

        return redirect()
            ->route('products.index');
    }
}
