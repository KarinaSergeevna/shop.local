<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Check extends Model
{
    use SoftDeletes;

    protected $guarded = [
        'id', 'created_at', 'updated_at', 'deleted_at'
    ];
    protected $dates = ['deleted_at'];

    // Get the orders associated with the check.
    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    // Get the check sum.
    public function getSum()
    {
        $orders = $this->orders;
        $totalSum = 0;

        if ($orders->count()) {
            foreach ($orders as $order) {
                $totalSum += $order->getSum();
            }
        }

        return $totalSum;
    }
}
