<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    protected $guarded = [
        'id', 'created_at', 'updated_at', 'deleted_at'
    ];
    protected $dates = ['deleted_at'];

    // Get the check associated with order.
    public function check()
    {
        return $this->belongsTo('App\Models\Check');
    }

    // Get the product associated with order.
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    // Get the order sum.
    public function getSum()
    {
        return $this->purchase_price * $this->quantity;
    }
}
