<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $guarded = [
        'id', 'created_at', 'updated_at', 'deleted_at'
    ];
    protected $dates = ['deleted_at'];

    // Get the unit associated with the product.
    public function unit()
    {
        return $this->belongsTo('App\Models\Unit');
    }

    // Get the category associated with the product.
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    // Get the orders associated with the product.
    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }
}
