<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [
        'id', 'created_at', 'updated_at'
    ];

    // Get all products associated with the category.
    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    // Get parent category of the category
    public function parentCategory()
    {
        return $this->hasOne('App\Models\Category', 'id', 'parent_id');
    }

    // Get all subcategories of the category
    public function subcategories()
    {
        return $this->hasMany('App\Models\Category', 'parent_id', 'id');
    }

    // Get all parent categories from database
    public function scopeGetParentCategories($query)
    {
        return $query->where('parent_id', '=', 0);
    }
}
