<?php
/*
 * Organizations routing file
 */
Route::group(['prefix' => 'organizations'], function() {
    Route::get('/', 'OrganizationsController@index')
        ->name('organizations.index');
    Route::get('/create', 'OrganizationsController@create')
        ->name('organizations.create');
    Route::post('/create', 'OrganizationsController@store')
        ->name('organizations.store');
    Route::get('/edit/{organization}', 'OrganizationsController@edit')
        ->name('organizations.edit');
    Route::post('/edit/{organization}', 'OrganizationsController@update')
        ->name('organizations.update');
    Route::get('/destroy/{organization}', 'OrganizationsController@destroy')
        ->name('organizations.destroy');
});