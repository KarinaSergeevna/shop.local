<?php

Route::group(['prefix' => 'products'], function() {
    Route::get('/', 'ProductsController@index')
        ->name('products.index');
    Route::get('/create', 'ProductsController@create')
        ->name('products.create');
    Route::post('/create', 'ProductsController@store')
        ->name('products.store');
    Route::get('/edit/{product}', 'ProductsController@edit')
        ->name('products.edit');
    Route::post('/edit/{product}', 'ProductsController@update')
        ->name('products.update');
    Route::get('/destroy/{product}', 'ProductsController@destroy')
        ->name('products.destroy');
});