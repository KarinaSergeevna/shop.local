<?php
/*
 * Categories routing file
 */
Route::group(['prefix' => 'categories'], function() {
    Route::get('/', 'CategoriesController@index')
        ->name('categories.index');
    Route::get('/create', 'CategoriesController@create')
        ->name('categories.create');
    Route::post('/create', 'CategoriesController@store')
        ->name('categories.store');
    Route::get('/subcategories/{category}', 'CategoriesController@getSubcategories')
        ->name('categories.subcategories');
    Route::get('/edit/{category}', 'CategoriesController@edit')
        ->name('categories.edit');
    Route::post('/edit/{category}', 'CategoriesController@update')
        ->name('categories.update');
    Route::get('/destroy/{category}', 'CategoriesController@destroy')
        ->name('categories.destroy');
});