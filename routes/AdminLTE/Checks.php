<?php

Route::group(['prefix' => 'checks'], function() {
    Route::get('/', 'ChecksController@index')
        ->name('checks.index');
    Route::get('/one/{check}', 'ChecksController@one')
        ->name('checks.one');
    Route::get('/create', 'ChecksController@create')
        ->name('checks.create');
    Route::post('/create', 'ChecksController@store')
        ->name('checks.store');
    Route::get('/edit/{check}', 'ChecksController@edit')
        ->name('checks.edit');
    Route::post('/edit/{check}', 'ChecksController@update')
        ->name('checks.update');
    Route::get('/toggle/{id}', 'ChecksController@toggle')
        ->name('checks.toggle');
    Route::get('/form-check/{id}', 'ChecksController@formCheck')
        ->name('checks.form-check');
    Route::get('/form-order/{id}', 'ChecksController@formOrder')
        ->name('checks.form-order');
    Route::get('/form-invoice/{id}', 'ChecksController@formInvoice')
        ->name('checks.form-invoice');
    Route::get('/download/{path}', 'ChecksController@download')
        ->name('checks.download');

});