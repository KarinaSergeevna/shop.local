<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([], function() {
    require_once('AdminLTE/Products.php');
    require_once('AdminLTE/Categories.php');
    require_once('AdminLTE/Checks.php');
    require_once('AdminLTE/Organizations.php');
});

// Rate routes
Route::get('/rate/update', 'RateController@update')
    ->name('rate.update.form');
Route::post('/rate/update', 'RateController@updatePost')
    ->name('rate.update.post');