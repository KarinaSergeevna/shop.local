<?php

return [
    'danger_modal_title' => 'Подтверждение удаления',
    'danger_modal_message' => 'Пожалуйста, подтвердите удаление.',
    'success_modal_title' => 'Подтверждение восстановлния',
    'success_modal_message' => 'Пожалуйста, подтвердите восстановление.',
];