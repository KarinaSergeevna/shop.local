<?php
/*
 * Языковой файл для сообщений в работе с товарами
 */
return [
    'creation_success' => 'Товар успешно создан.',
    'creation_failure' => 'Не удалось создать товар.',
    'updating_success' => 'Товар успешно изменен.',
    'updating_failure' => 'Не удалось изменить товар.',
    'deleting_success' => 'Товар успешно удален.',
    'deleting_failure' => 'Не удалось удалить товар.',
];