<?php
/*
 * Языковой файл для сообщений в работе с категориями
 */
return [
    'subcategories_existing_warning' => 'Категория :category_name имеет подкатегории. Сначала удалите их.',
    'creation_success' => 'Категория успешно создана.',
    'creation_failure' => 'Не удалось создать категорию.',
    'updating_success' => 'Категория успешно изменена.',
    'updating_failure' => 'Не удалось изменить категорию.',
    'deleting_success' => 'Категория успешно удалена.',
    'deleting_failure' => 'Не удалось удалить категорию.',
];