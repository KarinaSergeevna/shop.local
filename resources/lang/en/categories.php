<?php
/*
 * Language file for categories messages
 */
return [
    'subcategories_existing_warning' => 'Category :category_name has subcategories. Please, remove them.',
    'creation_success' => 'Category was created successfully.',
    'creation_failure' => 'Category wasn\'t created.',
    'updating_success' => 'Category was updated successfully.',
    'updating_failure' => 'Category wasn\'t updated.',
    'deleting_success' => 'Category was deleted successfully.',
    'deleting_failure' => 'Category wasn\'t deleted.',
];