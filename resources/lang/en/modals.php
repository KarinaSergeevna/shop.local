<?php

return [
    'danger_modal_title' => 'Removal confirmation',
    'danger_modal_message' => 'Please, confirm removing.',
    'success_modal_title' => 'Restoration confirmation',
    'success_modal_message' => 'Please, confirm restoration.',
];