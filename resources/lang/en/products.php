<?php
/*
 * Language file for products messages
 */
return [
    'creation_success' => 'Product was created successfully.',
    'creation_failure' => 'Product wasn\'t created.',
    'updating_success' => 'Product was updated successfully.',
    'updating_failure' => 'Product wasn\'t updated.',
    'deleting_success' => 'Product was deleted successfully.',
    'deleting_failure' => 'Product wasn\'t deleted.',
];