<?php
/*
 * Language file for organizations messages
 */
return [
    'creation_success' => 'Organization was created successfully.',
    'creation_failure' => 'Organization wasn\'t created.',
    'updating_success' => 'Organization was updated successfully.',
    'updating_failure' => 'Organization wasn\'t updated.',
    'deleting_success' => 'Organization was deleted successfully.',
    'deleting_failure' => 'Organization wasn\'t deleted.',
];