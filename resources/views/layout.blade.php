<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Тестовая страница</title>

        <!-- Styles -->
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="/css/font-awesome.min.css">
        <link rel="stylesheet" href="/css/styles.css">

        <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
        <script src="{{ asset('js/products.js') }}"></script>

    </head>
    <body>
        <div class="row">
            <div class="header">
                <div class="wrapper">
                    <div class="col-md-10">
                        <span class="logo">Предприятие Плюс</span>
                    </div>
                    <div class="col-md-2">
                        <span class="rate">Текущий курс: <a class="change_rate" href="{{ route('rate.update.form') }}"><strong>{{ number_format(App\Rate::first()->rate, 2) }}</strong></a></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="left_menu">
                <div class="wrapper">
                    <div class="col-md-10">
                        <a href="{{ route('products.index') }}"><span class="navigation">Товары</span></a>
                        <a href="{{ route('categories.index') }}"><span class="navigation">Категории</span></a>
                        <a href="#"><span class="navigation">Поставщики</span></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="wrapper">
                <div class="content">
                    @yield('content')
                </div>
            </div>
        </div>

    </body>
</html>