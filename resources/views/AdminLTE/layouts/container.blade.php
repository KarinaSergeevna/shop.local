@extends('AdminLTE.layouts.base')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
            @section('title')
                {{ $title }}
            @show
            </h1>
            @section('breadcrumbs')
                @include('AdminLTE.parts.breadcrumbs')
            @show
        </section>

        <section class="content container-fluid">
            @yield('inner-content')
        </section>
    </div>
@endsection   