<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title', 'Main Page')</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!--Styles-->
    <link rel="stylesheet" href="{{ adminlte_asset('/core/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ adminlte_asset('/core/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ adminlte_asset('/core/Ionicons/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ adminlte_asset('/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ adminlte_asset('/css/custom.css') }}">
    <link rel="stylesheet" href="{{ adminlte_asset('/css/skins/skin-blue.min.css') }}">

    @stack('styles')

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery 3 -->
    <script src="{{ adminlte_asset('core/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ adminlte_asset('js/custom.js') }}"></script>

    @stack('head-scripts')

    <!--Google Fonts-->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        @section('header')
            @include('AdminLTE.parts.header')
        @show

        @section('left-sidebar')
            @include('AdminLTE.parts.left-sidebar')
        @show

        @yield('content')

        @section('footer')
            @include('AdminLTE.parts.footer')
        @show

        @section('control-sidebar')
            @include('AdminLTE.parts.control-sidebar')
        @show
    </div>

    <!-- Bootstrap 3.3.7 -->
    <script src="{{ adminlte_asset('/core/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ adminlte_asset('/js/adminlte.min.js') }}"></script>

    @stack('bottom-scripts')
</body>
</html>