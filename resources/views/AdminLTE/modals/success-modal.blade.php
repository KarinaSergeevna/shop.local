<div class="modal modal-success fade" id="modal-success">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ trans('modals.success_modal_title') }}</h4>
            </div>
            <div class="modal-body">
                <p>{{ trans('modals.success_modal_message') }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Отменить</button>
                <a class="itemRoute" href=""><button type="button" class="btn btn-outline">Восстановить</button></a>
            </div>
        </div>
    </div>
</div>