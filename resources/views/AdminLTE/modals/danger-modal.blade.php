<div class="modal modal-danger fade" id="modal-danger">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{ trans('modals.danger_modal_title') }}</h4>
            </div>
            <div class="modal-body">
                <p>{{ trans('modals.danger_modal_message') }}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Отменить</button>
                <a class="itemRoute" href=""><button type="button" class="btn btn-outline">Удалить</button></a>
            </div>
        </div>
    </div>
</div>