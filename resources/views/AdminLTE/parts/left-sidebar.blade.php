<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ adminlte_asset('/img/avatar2.png') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">ПАНЕЛЬ УПРАВЛЕНИЯ</li>
            <li><a href="{{ route('products.index') }}"><i class="fa fa-link"></i> <span>Товары</span></a></li>
            <li><a href="{{ route('categories.index') }}"><i class="fa fa-link"></i> <span>Категории</span></a></li>
            <li><a href="{{ route('checks.index') }}"><i class="fa fa-link"></i> <span>Чеки</span></a></li>
            <li><a href="{{ route('organizations.index') }}"><i class="fa fa-link"></i> <span>Организации</span></a></li>
        </ul>
    </section>
</aside>
