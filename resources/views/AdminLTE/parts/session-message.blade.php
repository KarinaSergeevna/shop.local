<div class="row">
    <div class="col-xs-6">
        <div class="alert alert-{{ $status }}">
            {{ $message }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
</div>