@extends('AdminLTE.layouts.container')

@section('inner-content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
            <div class="box-header">
                <div class="adding-button">
                    <a href="{{ route('categories.create') }}" class="card-header-action">
                        <button class="btn btn-success btn-sm"><i class="fa fa-plus-square"></i> Добавить</button>
                    </a>
                </div>
            </div>
            <div class="box-body">
                @if(session('message'))
                    @include('AdminLTE.parts.session-message', ['message' => session('message'), 'status' => session('status')])
                @endif
                <table id="datatable" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th>Родительская категория</th>
                        <th class="text-center">Опции</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($parentCategories as $category)
                            <tr>
                                <td>{{ $category->name }}</td>
                                <td>-</td>
                                <td class="text-center">
                                    <a class="btn btn-info" href="{{ route('categories.edit', ['category' => $category->id]) }}">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <button class="btn btn-danger modalButton" value="{{ route('categories.destroy', ['category' => $category->id]) }}" type="button" data-toggle="modal" data-target="#modal-danger">
                                        <i class="fa fa-trash-o"></i>
                                    </button>
                                </td>
                            </tr>
                            @php($subcategories = $category->subcategories)
                            @if($subcategories->count())
                                @foreach($subcategories as $subcategory)
                                    <tr>
                                        <td>{{ $subcategory->name }}</td>
                                        <td>{{ $subcategory->parentCategory->name }}</td>
                                        <td class="text-center">
                                            <a class="btn btn-info" href="{{ route('categories.edit', ['category' => $subcategory->id]) }}">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <button class="btn btn-danger modalButton" value="{{ route('categories.destroy', ['category' => $subcategory->id]) }}" type="button" data-toggle="modal" data-target="#modal-danger">
                                                <i class="fa fa-trash-o"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Название</th>
                        <th>Родительская категория</th>
                        <th class="text-center">Опции</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        </div>
        </div>
    </div>
    @include('AdminLTE.modals.danger-modal')
@endsection

@push('bottom-scripts')
    <script src="{{ adminlte_asset('core/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ adminlte_asset('core/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- SlimScroll -->
    <script src="{{ adminlte_asset('core/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ adminlte_asset('core/fastclick/lib/fastclick.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ adminlte_asset('/js/adminlte.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ adminlte_asset('/js/demo.js') }}"></script>
    <script src="{{ adminlte_asset('js/datatables.js') }}"></script>
    <script>
        $(function() {
            var table1 = $('#datatable').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false,
                orderCellsTop: true,
                fixedHeader: true,
                "order": [
                    [0, "asc"]
                ],
            });

            $('#datatable thead tr').clone(true).appendTo('#datatable thead');
            $('#datatable thead tr:eq(1) th').each(function (i) {
                var title = $(this).text();
                if (i == 0 || i == 1) {
                    $(this).html('<input type="text" placeholder="Поиск..." />');

                    $('input', this).on('keyup change', function () {
                        if (table1.column(i).search() !== this.value) {
                            table1
                                .column(i)
                                .search(this.value)
                                .draw();
                        }
                    });
                } else {
                    $(this).html('');
                }
            });
        });
    </script>
@endpush