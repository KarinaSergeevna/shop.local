@extends('AdminLTE.layouts.container')

@section('inner-content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                @include('AdminLTE.forms.products-creating-editing-form')
            </div>
        </div>
    </div>
@endsection