@extends('AdminLTE.layouts.container')

@section('inner-content')
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">В наличии</a></li>
            <li><a href="#tab_2" data-toggle="tab">Дефектура</a></li>
            <li class="pull-right">
                <a href="{{ route('products.create') }}" class="card-header-action">
                    <button class="btn btn-success btn-sm"><i class="fa fa-plus-square"></i> Добавить</button>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">

                            </div>
                            <div class="box-body">
                                @if(session('message'))
                                    @include('AdminLTE.parts.session-message', ['message' => session('message'), 'status' => session('status')])
                                @endif
                                <table class="table table-bordered table-hover" id="datatable1">
                                    <thead>
                                    <tr>
                                        <th class="text-center">Наименование</th>
                                        <th class="text-center">К-во</th>
                                        <th class="text-center">Ед.</th>
                                        <th class="text-center">Категория</th>
                                        <th class="text-center">Мин. цена (грн)</th>
                                        <th class="text-center">Цена по курсу (грн)</th>
                                        <th class="text-center">Дата закупки</th>
                                        <th class="text-center">Опции</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($productsInStock as $product)
                                        <tr>
                                            <td class="name">
                                                {{ $product->name }}
                                            </td>
                                            <td class="text-center">
                                                {{ $product->quantity }}
                                            </td>
                                            <td class="text-center">
                                                @if($product->unit_id)
                                                    {{ $product->unit->name }}
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($product->category_id)
                                                    {{ $product->category->name }}
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                {{ $product->price_sales_minimal_uan }}
                                            </td>
                                            <td class="text-center">
                                                {{ round($product->price_sales_minimal_uan/$product->purchase_usd_rate * $rate, 2) }}
                                            </td>
                                            <td class="text-center">{{ formatDate($product->updated_at) }}
                                            </td>
                                            <td class="text-center">
                                                <a class="btn btn-info" href="{{ route('products.edit', ['product' => $product->id]) }}">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <button class="btn btn-danger modalButton" value="{{ route('products.destroy', ['product' => $product->id]) }}" type="button" data-toggle="modal" data-target="#modal-danger">
                                                    <i class="fa fa-trash-o"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th class="text-center">Наименование</th>
                                        <th class="text-center">К-во</th>
                                        <th class="text-center">Ед.</th>
                                        <th class="text-center">Категория</th>
                                        <th class="text-center">Мин. цена (грн)</th>
                                        <th class="text-center">Цена по курсу (грн)</th>
                                        <th class="text-center">Дата закупки</th>
                                        <th class="text-center">Опции</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="tab_2">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">
                            </div>
                            <div class="box-body">
                                <table class="table table-bordered table-hover" id="datatable2">
                                    <thead>
                                    <tr>
                                        <th class="text-center">Наименование</th>
                                        <th class="text-center">Ед.</th>
                                        <th class="text-center">Категория</th>
                                        <th class="text-center">Мин. цена (грн)</th>
                                        <th class="text-center">Цена по курсу (грн)</th>
                                        <th class="text-center">Дата закупки</th>
                                        <th class="text-center">Опции</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($productsMissing as $product)
                                        <tr>
                                            <td class="name">
                                                {{ $product->name }}
                                            </td>
                                            <td class="text-center">
                                                @if($product->unit_id)
                                                    {{ $product->unit->name }}
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if($product->category_id)
                                                    {{ $product->category->name }}
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                {{ $product->price_sales_minimal_uan }}
                                            </td>
                                            <td class="text-center">
                                                {{ round($product->price_sales_minimal_uan/$product->purchase_usd_rate * $rate, 2) }}
                                            </td>
                                            <td class="text-center">{{ formatDate($product->updated_at) }}
                                            </td>
                                            <td class="text-center">
                                                <a class="btn btn-info" href="{{ route('products.edit', ['product' => $product->id]) }}">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <button class="btn btn-danger modalButton" value="{{ route('products.destroy', ['product' => $product->id]) }}" type="button" data-toggle="modal" data-target="#modal-danger">
                                                    <i class="fa fa-trash-o"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th class="text-center">Наименование</th>
                                        <th class="text-center">Ед.</th>
                                        <th class="text-center">Категория</th>
                                        <th class="text-center">Мин. цена (грн)</th>
                                        <th class="text-center">Цена по курсу (грн)</th>
                                        <th class="text-center">Дата закупки</th>
                                        <th class="text-center">Опции</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('AdminLTE.modals.danger-modal')
@endsection

@push('bottom-scripts')
    <script src="{{ adminlte_asset('core/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ adminlte_asset('core/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- SlimScroll -->
    <script src="{{ adminlte_asset('core/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ adminlte_asset('core/fastclick/lib/fastclick.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ adminlte_asset('/js/adminlte.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ adminlte_asset('/js/demo.js') }}"></script>
    <script src="{{ adminlte_asset('js/datatables.js') }}"></script>
    <script>
        $(function() {
            var table1 = $('#datatable1').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : true,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false,
                orderCellsTop: true,
                fixedHeader: true,
                "order": [
                    [0, "asc"]
                ],
            });

            $('#datatable1 thead tr').clone(true).appendTo('#datatable1 thead');
            $('#datatable1 thead tr:eq(1) th').each(function (i) {
                var title = $(this).text();
                if (i == 0 || i == 3 || i == 6) {
                    $(this).html('<input type="text" placeholder="Поиск..." />');

                    $('input', this).on('keyup change', function () {
                        if (table1.column(i).search() !== this.value) {
                            table1
                                .column(i)
                                .search(this.value)
                                .draw();
                        }
                    });
                }
                else {
                    $(this).html('');
                }
            });

            var table2 = $('#datatable2').DataTable({
                'paging'      : true,
                'lengthChange': true,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false,
                orderCellsTop: true,
                fixedHeader: true,
                "order": [
                    [0, "asc"]
                ],
            });

            $('#datatable2 thead tr').clone(true).appendTo('#datatable2 thead');
            $('#datatable2 thead tr:eq(1) th').each(function (i) {
                var title = $(this).text();
                if (i == 0 || i == 2 || i == 5) {
                    $(this).html('<input type="text" placeholder="Поиск..." />');

                    $('input', this).on('keyup change', function () {
                        if (table2.column(i).search() !== this.value) {
                            table2
                                .column(i)
                                .search(this.value)
                                .draw();
                        }
                    });
                }
                else {
                    $(this).html('');
                }
            });
        });
    </script>
@endpush