@extends('AdminLTE.layouts.container')

@section('inner-content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Название</th>
                                            <th>К-во</th>
                                            <th>Цена</th>
                                            <th>Сумма</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($checkOrders as $order)
                                            <tr>
                                                <td>{{ $order->product->name }}</td>
                                                <td>{{ $order->quantity }}</td>
                                                <td>{{ number_format($order->purchase_price, 2, ',', ' ') }}</td>
                                                <td>{{ number_format($order->purchase_price * $order->quantity, 2, ',', ' ') }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <a href="{{ $checkPath }}">
                                    <button type="button" class="btn btn-success btn-sm"><i class="fa fa-ban" aria-hidden="true"></i> Загрузить чек</button>
                                </a>
                                <a href="{{ $orderPath }}">
                                    <button type="button" class="btn btn-primary btn-sm" name="button_type"><i class="fa fa-bullseye" aria-hidden="true"></i> Загрузить накладную</button>
                                </a>
                                <a href="{{ $invoicePath }}">
                                    <button type="button" class="btn btn-warning btn-sm"><i class="fa fa-ban" aria-hidden="true"></i> Загрузить счет-фактуру</button>
                                </a>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection