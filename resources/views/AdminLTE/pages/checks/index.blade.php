@extends('AdminLTE.layouts.container')

@section('inner-content')
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">Активные</a></li>
            <li><a href="#tab_2" data-toggle="tab">Удаленные</a></li>
            <li class="pull-right">
                <a href="{{ route('checks.create') }}" class="card-header-action">
                    <button class="btn btn-success btn-sm"><i class="fa fa-plus-square"></i> Добавить</button>
                </a>
            </li>
        </ul>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"></div>
                    <div class="box-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                <table id="datatable1" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center">№</th>
                                            <th class="text-center">Дата и время</th>
                                            <th class="text-center">Сумма</th>
                                            <th class="text-center">Опции</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($checksActive as $check)
                                        <tr>
                                            <td class="text-center">
                                                {{ $check->id }}
                                            </td>
                                            <td class="text-center">
                                                {{ $check->updated_at }}
                                            </td>
                                            <td class="text-center">
                                                {{ number_format($check->getSum(), 2, ',', ' ') }}
                                            </td>
                                            <td class="text-center">
                                                <a class="btn btn-warning" href="{{ route('checks.one', ['check' => $check->id]) }}">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                <a class="btn btn-info" href="{{ route('checks.edit', ['check' => $check->id]) }}">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <button class="btn btn-danger modalButton" value="{{ route('checks.toggle', ['id' => $check->id]) }}" type="button" data-toggle="modal" data-target="#modal-danger">
                                                    <i class="fa fa-trash-o"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th class="text-center">№</th>
                                        <th class="text-center">Дата и время</th>
                                        <th class="text-center">Сумма</th>
                                        <th class="text-center">Опции</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <div class="tab-pane" id="tab_2">
                                <table id="datatable2" class="table table-bordered table-hover datatable">
                                    <thead>
                                    <tr>
                                        <th class="text-center">№</th>
                                        <th class="text-center">Дата и время</th>
                                        <th class="text-center">Сумма</th>
                                        <th class="text-center">Опции</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($checksDeleted as $check)
                                        <tr>
                                            <td class="text-center">
                                                {{ $check->id }}
                                            </td>
                                            <td class="text-center">
                                                {{ $check->updated_at }}
                                            </td>
                                            <td class="text-center">
                                                {{ number_format($check->getSum(), 2, ',', ' ') }}
                                            </td>
                                            <td class="text-center">
                                                <a class="btn btn-warning" href="{{ route('checks.one', ['check' => $check->id]) }}">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                <button class="btn btn-success modalButton" value="{{ route('checks.toggle', ['id' => $check->id]) }}" type="button" data-toggle="modal" data-target="#modal-success">
                                                    <i class="fa fa-arrow-circle-up"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th class="text-center">№</th>
                                        <th class="text-center">Дата и время</th>
                                        <th class="text-center">Сумма</th>
                                        <th class="text-center">Опции</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('AdminLTE.modals.danger-modal')
    @include('AdminLTE.modals.success-modal')
@endsection

@push('bottom-scripts')
<script src="{{ adminlte_asset('core/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ adminlte_asset('core/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ adminlte_asset('core/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ adminlte_asset('core/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ adminlte_asset('/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ adminlte_asset('/js/demo.js') }}"></script>
<script src="{{ adminlte_asset('js/datatables.js') }}"></script>
<script>
    $(function() {
        var table1 = $('#datatable1').DataTable({
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false,
            orderCellsTop: true,
            fixedHeader: true,
            "order": [
                [0, "desc"]
            ],
        });

        $('#datatable1 thead tr').clone(true).appendTo('#datatable1 thead');
        $('#datatable1 thead tr:eq(1) th').each(function (i) {
            var title = $(this).text();
            if (i == 0 || i == 1) {
                $(this).html('<input type="text" placeholder="Поиск..." />');

                $('input', this).on('keyup change', function () {
                    if (table1.column(i).search() !== this.value) {
                        table1
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
            }
            else {
                $(this).html('');
            }
        });

        var table2 = $('#datatable2').DataTable({
            'paging'      : true,
            'lengthChange': true,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false,
            orderCellsTop: true,
            fixedHeader: true,
            "order": [
                [0, "desc"]
            ],
        });

        $('#datatable2 thead tr').clone(true).appendTo('#datatable2 thead');
        $('#datatable2 thead tr:eq(1) th').each(function (i) {
            var title = $(this).text();
            if (i == 0 || i == 1) {
                $(this).html('<input type="text" placeholder="Поиск..." />');

                $('input', this).on('keyup change', function () {
                    if (table2.column(i).search() !== this.value) {
                        table2
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
            }
            else {
                $(this).html('');
            }
        });
    });
</script>
@endpush