@extends('layout')

@section('content')
    <h3>РЕДАКТИРОВАНИЕ КУРСА</h3>
    <hr>
    @include('AdminLTE.forms.update_rate_form')
@endsection