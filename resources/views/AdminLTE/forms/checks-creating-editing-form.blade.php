<div class="box">
    <div class="box-body">
        <div class="row">
            <div class="col-md-6">
                <table id="datatable1" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th class="d-none">id</th>
                        <th>Название</th>
                        <th>К-во на складе</th>
                        <th>Цена по курсу</th>
                        <th>Опции</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <td class="d-none" id="product_id_{{ $product->id }}">
                                {{ $product->id }}
                            </td>
                            <td id="product_name_{{ $product->id }}">
                                {{ $product->name }}
                            </td>
                            <td id="product_quantity_{{ $product->id }}">
                                {{ $product->quantity }}
                            </td>
                            <td id="product_price_{{ $product->id }}">
                                {{ round($product->price_sales_minimal_uan/$product->purchase_usd_rate * $rate, 2) }}
                            </td>
                            <td class="text-center">
                                <button class="btn btn-success choose-product-button btn-xs" type="button" value="{{ $product->id }}">
                                    <i class="fa fa-plus-square"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-md-6">
                <h4>Чек</h4>
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th>К-во</th>
                        <th>Цена</th>
                        <th>Сумма</th>
                        <th>Опции</th>
                    </tr>
                    </thead>
                    <tbody id="append">
                        @isset($check)
                            @foreach($orders as $order)
                                <tr>
                                    <td class="product-id d-none">
                                        {{ $order->product->id }}
                                    </td>
                                    <td class="product-name">
                                        {{ $order->product->name }}
                                    </td>
                                    <td>
                                        <input class="product-quantity" type="number" value="{{ $order->quantity }}"/>
                                    </td>
                                    <td class="product-price">
                                        {{ $order->purchase_price }}
                                    </td>
                                    <td class="product-sum">
                                        {{ $order->purchase_price * $order->quantity }}
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-danger btn-xs remove-row" type="button"><i class="fa fa-trash-o"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                        @endisset
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th>Итого: </th>
                            <th id="summary"> @isset($check) {{ $check->getSum() }} @endisset</th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
                <button type="button" id="save-check" class="btn btn-primary btn-sm" name="button_type"
                        @if(isset($check))
                            value="{{ route('checks.update', ['check' => $check->id]) }}"
                        @else
                            value="{{ route('checks.create') }}"
                        @endif
                >
                    <i class="fa fa-dot-circle-o" aria-hidden="true"></i> Сохранить
                </button>
                <a id="checkPath" href="{{ $checkPath }}"><button type="button" class="btn btn-success btn-sm"  @if(!isset($check)) disabled @endif><i class="fa fa-ban" aria-hidden="true"></i> Загрузить</button></a>
            </div>
        </div>
    </div>
</div>

@push('bottom-scripts')
<script src="{{ adminlte_asset('core/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ adminlte_asset('core/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- SlimScroll -->
<script src="{{ adminlte_asset('core/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ adminlte_asset('core/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ adminlte_asset('/js/adminlte.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ adminlte_asset('/js/demo.js') }}"></script>
<script src="{{ adminlte_asset('js/datatables.js') }}"></script>
<script>
    $(function() {
        var table1 = $('#datatable1').DataTable({
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false,
            orderCellsTop: true,
            fixedHeader: true,
            "order": [
                [0, "asc"]
            ],
            "pageLength": 25,
        });

        $('#datatable1 thead tr').clone(true).appendTo('#datatable1 thead');
        $('#datatable1 thead tr:eq(1) th').each(function (i) {
            var title = $(this).text();
            if (i == 1) {
                $(this).html('<input type="text" placeholder="Поиск..." />');

                $('input', this).on('keyup change', function () {
                    if (table1.column(i).search() !== this.value) {
                        table1
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
            }
            else {
                $(this).html('');
            }
        });

        $('.choose-product-button').on('click', function() {
            var product_id = $(this).val();
            var name = $('#product_name_' + product_id).html();
            var price = $('#product_price_' + product_id).html();
            var max_quantity = parseInt($('#product_quantity_' + product_id).html());

            $('tbody#append').append(
                '<tr><td class="product-id d-none">' + product_id +'</td>' +
                '<td class="product-name">' + name + '</td>' +
                '<td><input class="product-quantity" type="number" value="0" max="' + max_quantity + '"></td>' +
                '<td class="product-price">' + price + '</td>' +
                '<td class="product-sum">0.00</td>' +
                '<td class="text-center"><button class="btn btn-danger btn-xs remove-row" type="button"><i class="fa fa-trash-o"></i></button></td></tr>'
            );
        });

        $('tbody#append').on('input', 'input', function(){
            var inputValue = parseInt($(this).val());
            if (inputValue > $(this).attr('max')) {
                $(this).val($(this).attr('max'));
            }

            var sum = (Math.ceil($(this).parent().parent().find('.product-price').text() * $(this).val() * 100)/100).toFixed(2);
            $(this).parent().parent().find('.product-sum').text(sum);

            countTotalSum()
        });

        $('tbody#append').on('click', 'button.remove-row', function() {
            $(this).parent().parent().remove();

            countTotalSum();
        });

        function countTotalSum()
        {
            var sumTotal = 0;
            var inputs = $('.product-sum');
            $.each(inputs, function(index, value){
                sumTotal += parseFloat($(value).text());
            });

            $('#summary').text(sumTotal);
        }

        $('#save-check').on('click', function() {
            var csrftoken = $('meta[name="csrf-token"]').attr('content');
            var check = [];
            var rows = $('#append tr');
            $.each(rows, function(index, value) {
                var order = {};
                var row = $(value);
                order.product_id = row.find('.product-id').text();
                order.quantity = row.find('.product-quantity').val();
                order.price = row.find('.product-price').text();
                check.push(order);
            });

            if (check.length > 0) {
                $.ajax({
                    url: $(this).val(),
                    type: "POST",
                    cache: false,
                    data: {
                        '_token': csrftoken,
                        'items': check
                    },
                    success: function (data) {
                        $('a#checkPath').attr('href', data).attr('disabled', false);
                    },
                    error: function () {
                        $('#image_error').addClass('d-block').html('Не удалось удалить изображение.');
                    }
                });
            }
        });
    });
</script>
@endpush