<form action="" method="post">
    {{ csrf_field() }}
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="name">Текущий курс: </label>
            <input type="rate" class="form-control" id="rate" name="rate" value="{{ number_format($rate->rate, 2) }}">
        </div>
        @if ($errors->has('rate'))
            <div class="alert alert-danger">
                <strong> {{ $errors->first('rate') }} </strong>
            </div>
        @endif
    </div>
</div>
    <button type="submit" class="btn btn-default">Сохранить</button>
</form>