<form action="" method="post" role="form">
    <div class="box-body">
        {{ csrf_field() }}
        @if(session('message'))
            @include('AdminLTE.parts.session-message', ['message' => session('message'), 'status' => session('status')])
        @endif

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">Название категории * </label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ old('name') ?? $category->name ?? '' }}" required>
                </div>
                @if ($errors->has('name'))
                    <div class="alert alert-danger">
                        <strong> {{ $errors->first('name') }} </strong>
                    </div>
                @endif
                
                <div class="form-group">
                    <label for="parent_id">Родительская категория</label>
                    <select class="form-control" id="parent_id" name="parent_id">
                        <option value="0">Отсутствует</option>
                        @foreach($parentCategories as $one)
                            <option value="{{ $one->id }}" @if(isset($category) && $category->parent_id == $one->id) selected @endif>{{ $one->name }}</option>
                        @endforeach
                    </select>
                </div>

                <button type="submit" class="btn btn-primary btn-sm" name="button_type" value="save"><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Сохранить</button>
                @if(isset($category))
                    <button type="submit" class="btn btn-success btn-sm" name="button_type" value="apply"><i class="fa fa-bullseye" aria-hidden="true"></i> Применить</button>
                @endif
                <a href="{{ route('categories.index') }}">
                    <button type="button" class="btn btn-danger btn-sm"><i class="fa fa-ban" aria-hidden="true"></i> Отменить</button>
                </a>
            </div>
        </div>
    </div>
</form>