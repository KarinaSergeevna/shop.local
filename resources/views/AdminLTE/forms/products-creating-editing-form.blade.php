<form action="" method="post" role="form">
    <div class="box-body">
    {{ csrf_field() }}
        @if(session('message'))
            @include('AdminLTE.parts.session-message', ['message' => session('message'), 'status' => session('status')])
        @endif

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="name">Наименование * </label>
                <input type="name" class="form-control" id="name" name="name" value="{{ old('name') ?? $product->name ?? '' }}" required>
            </div>
            @if ($errors->has('name'))
                <div class="alert alert-danger">
                    <strong> {{ $errors->first('name') }} </strong>
                </div>
            @endif

            <div class="form-group">
                <label for="quantity">Количество *</label>
                <input type="number" class="form-control" id="quantity" name="quantity" value="{{ old('quantity') ?? $product->quantity ?? '' }}" required>
            </div>
            @if ($errors->has('quantity'))
                <div class="alert alert-danger">
                    <strong> {{ $errors->first('quantity') }} </strong>
                </div>
            @endif

            <div class="form-group">
                <label for="unit_id">Единицы измерения *</label>
                <select class="form-control" id="unit_id" name="unit_id">
                    @foreach($units as $unit)
                        <option value="{{ $unit->id }}" @if(isset($product) && $product->unit_id == $unit->id) selected @endif>{{ $unit->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="purchase_usd_rate">Курс закупки (USD) *</label>
                <input type="text" class="form-control" id="purchase_usd_rate" name="purchase_usd_rate" value="{{ number_format($rate, 2) }}" required>
            </div>
            @if ($errors->has('purchase_usd_rate'))
                <div class="alert alert-danger">
                    <strong> {{ $errors->first('purchase_usd_rate') }} </strong>
                </div>
            @endif

            <div class="form-group">
                <label for="price_purchase_usd">Цена закупки (USD):</label>
                <input type="text" class="form-control" id="price_purchase_usd" name="price_purchase_usd" value="{{ old('price_purchase_usd') ?? $product->price_purchase_usd ?? '' }}">
            </div>
            @if ($errors->has('price_purchase_usd'))
                <div class="alert alert-danger">
                    <strong> {{ $errors->first('price_purchase_usd') }} </strong>
                </div>
            @endif

            <div class="form-group">
                <label for="price_purchase_uan">Цена закупки (UAH) *</label>
                <input type="text" class="form-control" id="price_purchase_uan" name="price_purchase_uan" value="{{ old('price_purchase_uan') ?? $product->price_purchase_uan ?? '' }}" required>
            </div>
            @if ($errors->has('price_purchase_uan'))
                <div class="alert alert-danger">
                    <strong> {{ $errors->first('price_purchase_uan') }} </strong>
                </div>
            @endif
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="percent">Наценка (%) *</label>
                <input type="number" class="form-control" id="percent" name="percent" value="{{ old('percent') ?? $product->percent ?? '' }}" required>
            </div>
            @if ($errors->has('percent'))
                <div class="alert alert-danger">
                    <strong> {{ $errors->first('percent') }} </strong>
                </div>
            @endif

            <div class="form-group">
                <label for="price_sales_minimal_uan">Цена продажи (UAH) * </label>
                <input type="text" class="form-control" id="price_sales_minimal_uan" name="price_sales_minimal_uan" value="{{ old('price_sales_minimal_uan') ?? $product->price_sales_minimal_uan ?? '' }}" required>
            </div>
            @if ($errors->has('price_sales_minimal_uan'))
                <div class="alert alert-danger">
                    <strong> {{ $errors->first('price_sales_minimal_uan') }} </strong>
                </div>
            @endif

            <div class="form-group">
                <label for="description">Описание </label>
                <textarea class="form-control" rows="5" id="description" name="description">{{ old('description') ?? $product->description ?? '' }}</textarea>
            </div>
            @if ($errors->has('description'))
                <div class="alert alert-danger">
                    <strong> {{ $errors->first('description') }} </strong>
                </div>
            @endif

            <div class="form-group">
                <label for="category_id">Категория *</label>
                <select class="form-control" id="category_id" name="category_id" onchange="select_category($(this).val())">
                    <option value="0">-</option>
                    @foreach($parentCategories as $category)
                        <option value="{{ $category->id }}"
                            @isset($product)
                                @if($product->category->parent_id === 0 && $product->category->id === $category->id)
                                    selected
                                @elseif($product->category->parent_id !== 0 && $product->category->parentCategory->id === $category->id)
                                    selected
                                @endif
                            @endisset
                        >{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
            <div id="div-subcat" class="form-group hidden">
                <label for="subcat">Подкатегория: </label>
                <select id="subcat" name="subcat" class="form-control">

                </select>
            </div>
        </div>
    </div>
        <button type="submit" class="btn btn-primary btn-sm" name="button_type" value="save"><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Сохранить</button>
        @if(isset($product))
            <button type="submit" class="btn btn-success btn-sm" name="button_type" value="apply"><i class="fa fa-bullseye" aria-hidden="true"></i> Применить</button>
        @endif
        <a href="{{ route('products.index') }}">
            <button type="button" class="btn btn-danger btn-sm"><i class="fa fa-ban" aria-hidden="true"></i> Отменить</button>
        </a>
    </div>
</form>

@push('bottom-scripts')
    <script>
        $(document).ready(function(){
            select_category($('#category_id').val(),
                '<?php if(isset($product) && $product->category){
                    echo $product->category->id;
                } else {
                    echo null;
                } ?>');

            $('#percent').val(Math.round($('#price_sales_minimal_uan').val()/$('#price_purchase_uan').val() * 100) - 100);
        });

        $('#purchase_usd_rate').on('input', function(){
            var price_purchase_usd = $('#price_purchase_usd').val();
            if(price_purchase_usd) {
                var price = Math.ceil(price_purchase_usd * $(this).val() * 100) / 100;
                $('#price_purchase_uan').val(price);
                priceSalesMinimalUah();
            }
        });

        $('#price_purchase_usd').on('input', function(){
            var price = Math.ceil($(this).val() * $('#purchase_usd_rate').val() * 100)/100;
            $('#price_purchase_uan').val(price);
            priceSalesMinimalUah();
        });

        $('#price_purchase_uan').on('input', function(){
            var price = Math.ceil($(this).val() / $('#purchase_usd_rate').val() * 100)/100;
            $('#price_purchase_usd').val(price);
            priceSalesMinimalUah();
        });

        $('#percent').on('input', function(){
            priceSalesMinimalUah();
        });

        function priceSalesMinimalUah() {
            var percent = (parseFloat($('#percent').val()) + 100)/100;
            var price_purchase_uan = $('#price_purchase_uan').val();
            if(percent && price_purchase_uan) {
                var price_min = Math.ceil(percent * price_purchase_uan * 100) / 100;
                $('#price_sales_minimal_uan').val(price_min);
            }
        }
    </script>
@endpush