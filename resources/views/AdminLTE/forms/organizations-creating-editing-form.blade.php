<form action="" method="post" role="form">
    <div class="box-body">
        {{ csrf_field() }}
        @if(session('message'))
            @include('AdminLTE.parts.session-message', ['message' => session('message'), 'status' => session('status')])
        @endif

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">Название * </label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ old('name') ?? $organization->name ?? '' }}" required>
                </div>
                @if ($errors->has('name'))
                    <div class="alert alert-danger">
                        <strong> {{ $errors->first('name') }} </strong>
                    </div>
                @endif

                <div class="form-group">
                    <label for="address">Адрес </label>
                    <input type="text" class="form-control" id="address" name="address" value="{{ old('address') ?? $organization->address ?? '' }}">
                </div>
                @if ($errors->has('address'))
                    <div class="alert alert-danger">
                        <strong> {{ $errors->first('address') }} </strong>
                    </div>
                @endif

                <div class="form-group">
                    <label for="phone">Телефон </label>
                    <input type="text" class="form-control" id="phone" name="phone" value="{{ old('phone') ?? $organization->phone ?? '' }}">
                </div>
                @if ($errors->has('phone'))
                    <div class="alert alert-danger">
                        <strong> {{ $errors->first('phone') }} </strong>
                    </div>
                @endif

                <div class="form-group">
                    <label for="contact_person">Контактное лицо </label>
                    <input type="text" class="form-control" id="contact_person" name="contact_person" value="{{ old('contact_person') ?? $organization->contact_person ?? '' }}">
                </div>
                @if ($errors->has('contact_person'))
                    <div class="alert alert-danger">
                        <strong> {{ $errors->first('contact_person') }} </strong>
                    </div>
                @endif

                <div class="form-group">
                    <label for="document">Документ *</label>
                    <input type="text" class="form-control" id="document" name="document" value="{{ old('document') ?? $organization->document ?? '' }}" placeholder="Договор №...">
                </div>
                @if ($errors->has('document'))
                    <div class="alert alert-danger">
                        <strong> {{ $errors->first('document') }} </strong>
                    </div>
                @endif

                <div class="form-group">
                    <label for="comment">Комментарий </label>
                    <textarea class="form-control" rows="3" id="comment" name="comment">{{ old('comment') ?? $organization->comment ?? '' }}</textarea>
                </div>
                @if ($errors->has('comment'))
                    <div class="alert alert-danger">
                        <strong> {{ $errors->first('comment') }} </strong>
                    </div>
                @endif

                <button type="submit" class="btn btn-primary btn-sm" name="button_type" value="save"><i class="fa fa-dot-circle-o" aria-hidden="true"></i> Сохранить</button>
                @if(isset($organization))
                    <button type="submit" class="btn btn-success btn-sm" name="button_type" value="apply"><i class="fa fa-bullseye" aria-hidden="true"></i> Применить</button>
                @endif
                <a href="{{ route('categories.index') }}">
                    <button type="button" class="btn btn-danger btn-sm"><i class="fa fa-ban" aria-hidden="true"></i> Отменить</button>
                </a>
            </div>
        </div>
    </div>
</form>