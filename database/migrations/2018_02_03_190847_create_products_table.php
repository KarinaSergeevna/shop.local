<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->float('quantity')->default(0);
            $table->float('purchase_usd_rate');
            $table->float('price_purchase_usd')->nullable();
            $table->float('price_purchase_uan');
            $table->float('price_sales_minimal_uan');
            $table->integer('category_id')->nullable()->unsigned();
            $table->integer('provider_id')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }


    public function down()
    {
        Schema::dropIfExists('products');
    }
}
