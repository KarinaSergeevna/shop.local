$(function() {
    $('button.modalButton').on('click', function () {
        $('a.itemRoute').attr('href', $(this).val());
    });
});

function select_category(id, subcategory = null) {
    var subcategories_wrapper = $('#div-subcat');
    var subcategories_container = $('#subcat');

    subcategories_container.text('');
    subcategories_wrapper.addClass('hidden');

    $.ajax({
        url: '/categories/subcategories/' + id,
        type: 'get',
        dataType: 'html',
        success: function(data) {
            var current_subcategory = (subcategory) ? subcategory : $('#product_subcat').val();
            var object = JSON.parse(data);

            if (object[0]) {
                subcategories_wrapper.removeClass('hidden');
                subcategories_container.append("<option value='all'>" + "ВСЕ" + "</option>");

                for (var el, i = 0; i < object.length; i++) {
                    el = object[i];
                    let option_status = (el['id'] == current_subcategory) ? 'selected' : '';
                    subcategories_container.append("<option " + option_status + " value='" + el.id + "'>" + el.name + "</option>");
                }
            }
        }
    });
}
