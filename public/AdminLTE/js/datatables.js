/**
 * --------------------------------------------------------------------------
 * CoreUI Pro Boostrap Admin Template (2.0.0): datatables.js
 * Licensed under MIT (https://coreui.io/license)
 * --------------------------------------------------------------------------
 */
$.extend( $.fn.dataTable.defaults, {
    "pageLength": 50,
    "language": {
        "processing": "Подождите...",
        "search": "Поиск:",
        "lengthMenu": "Показать _MENU_ записей",
        "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
        "infoEmpty": "Записи с 0 до 0 из 0 записей",
        "infoFiltered": "(отфильтровано из _MAX_ записей)",
        "infoPostFix": "",
        "loadingRecords": "Загрузка записей...",
        "zeroRecords": "Записи отсутствуют.",
        "emptyTable": "В таблице отсутствуют данные",
        "paginate": {
            "first": "Первая",
            "previous": "Предыдущая",
            "next": "Следующая",
            "last": "Последняя"
        },
        "aria": {
            "sortAscending": ": активировать для сортировки столбца по возрастанию",
            "sortDescending": ": активировать для сортировки столбца по убыванию"
        },
    },
    "columnDefs": [
        { "targets": 'dt-no-sort', "orderable": false},
        { "targets": 'dt-hidden', "visible": false, "searchable": false }
    ],
    "order": [
        [ 0, "desc" ]
    ],
    'search': false
});
$('.datatable').attr('style', 'border-collapse: collapse !important');
//# sourceMappingURL=datatables.js.map

